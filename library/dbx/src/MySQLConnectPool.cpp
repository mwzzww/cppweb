#ifndef XG_DB_MYSQLCONNECTPOOL_CPP
#define XG_DB_MYSQLCONNECTPOOL_CPP
//////////////////////////////////////////////////////////////
#include "../MySQLConnect.h"
#include "../DBConnectPool.h"

class MySQLConnectPool : public DBConnectPool
{
public:
	MySQLConnectPool()
	{
		MySQLConnect::Setup();
	}
	
protected:
	DBConnect* createConnect()
	{
		MySQLConnect* conn = new MySQLConnect();

		if (conn->init(cfg)) return conn;
		
		delete conn;

		return NULL;
	}
};

DEFINE_DBCONNECTPOOL_EXPORT_FUNC(MySQLConnectPool)

//////////////////////////////////////////////////////////////
#endif