package webx.http;

import webx.json.JsonObject;

public class HttpResponse{
	protected byte[] body = null;
	protected HttpHeadNode head = new HttpHeadNode();

    public byte[] getBody(){
        return body;
    }
	public byte[] getBytes(){
		byte[] res = null;
		byte[] tmp = null;
		
		try{
			tmp = (head.toString() + "\r\n\r\n").getBytes(Http.GetCharset());
		}
		catch(Exception e){
            e.printStackTrace();
		}

		if (tmp == null || body == null || body.length <= 0) return tmp;

		res = new byte[tmp.length + body.length];

		System.arraycopy(tmp, 0, res, 0, tmp.length);
		System.arraycopy(body, 0, res, tmp.length, body.length);

		return res;
	}
	public void setBody(String msg){
		try{
			this.body = msg.getBytes(Http.GetCharset());
		}
		catch(Exception e){
		    e.printStackTrace();
		}
	}
    public void setBody(byte[] msg){
        this.body = msg;
    }
    public void setBody(JsonObject json){
        setBody(json.toString());
    }
    public void setBody(Object obj) throws IllegalAccessException{
        setBody(JsonObject.FromObject(obj).toString());
    }

	public String getHeader(String key){
		return head.get(key);
	}
	public void setHeader(String key, String val){
		head.set(key, val);
	}
}
