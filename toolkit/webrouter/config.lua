-- 监听端口
PORT = 5555

-- 主机地址
HOST = "0.0.0.0"

-- SSL监听端口
SSL_PORT = 7777

-- PEM证书文件
CERT_FILEPATH = "cert.crt"

-- PEM私钥文件
PRIKEY_FILEPATH = "cert.key"

-- 注册中心端口
ROUTE_PORT = 8888

-- 注册中心地址
ROUTE_HOST = "127.0.0.1"

-- 路由接口权限
ROUTE_ACCESS = "public"

-- 目的地址列表(地址:端口:权重|地址:端口:权重)
DEST_HOST_LIST = "127.0.0.1:8888"

-- 处理线程数
THREAD_SIZE = 32

-- 跨域主机列表(地址|地址)
ALLOW_ORIGIN = "*"

-- 地址白名单(地址|地址)
WHITE_HOST_LIST = ""

-- 地址黑名单(地址|地址)
BLACK_HOST_LIST = ""

-- 同一地址每分钟请求上限
ADDRESS_REQUEST_PERMIN = 1000

-- 日志存放目录
LOGFILE_PATH = "log"

-- 日志级别(0-DEBUG 1-TIPS 2-INFO 3-IMPORTANT 4-ERROR)
LOGFILE_LEVEL = 2

-- 单个日志文件最大大小(KB)
LOGFILE_MAXSIZE = 10000