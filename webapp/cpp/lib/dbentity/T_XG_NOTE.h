#ifndef XG_ENTITY_CT_XG_NOTE_H
#define XG_ENTITY_CT_XG_NOTE_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_NOTE : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	int update(bool nullable = false);
	string getValue(const string& key);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(const string& condition, bool nullable = false, const vector<DBData*>& vec = {});

	template<class DATA_TYPE, class ...ARGS>
	int remove(const string& condition, const DATA_TYPE& val, ARGS ...args)
	{
		vector<sp<DBData>> vec;

		DBConnect::Pack(vec, val, args...);

		vector<DBData*> tmp;

		for (auto& item : vec) tmp.push_back(item.get());

		return remove(condition, tmp);
	}
	template<class DATA_TYPE, class ...ARGS>
	sp<QueryResult> find(const string& condition, const DATA_TYPE& val, ARGS ...args)
	{
		vector<sp<DBData>> vec;

		DBConnect::Pack(vec, val, args...);

		vector<DBData*> tmp;

		for (auto& item : vec) tmp.push_back(item.get());
	
		sp<QueryResult> res = find(condition, tmp);

		if (res)
		{
			for (auto& item : vec) res->hold(item);
		}

		return res;
	}

public:
	DBString	id;
	DBInteger	type;
	DBString	user;
	DBString	folder;
	DBString	title;
	DBBlob		icon;
	DBBlob		content;
	DBInteger	level;
	DBString	remark;
	DBInteger	position;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_NOTE";
	}

	static const char* GetColumnString()
	{
		return "ID,TYPE,USER,FOLDER,TITLE,ICON,CONTENT,LEVEL,REMARK,POSITION,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
