#include <webx/menu.h>
#include <dbentity/T_XG_MENU.h>

class GetMenuList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetMenuList)

int GetMenuList::process()
{
	param_name_string(folder);
	
	checkLogin();

	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	stdx::format(sqlcmd, "SELECT ID,URL,ICON,TITLE,ENABLED,STATETIME FROM T_XG_MENU WHERE FOLDER='%s' AND ENABLED>0 ORDER BY POSITION ASC", folder.c_str());

	sp<RowData> row;
	sp<QueryResult> rs = dbconn->query(sqlcmd);

	if (!rs) return simpleResponse(XG_SYSERR);


	int res = 0;
	JsonElement arr = json.addArray("list");
	
	while (row = rs->next())
	{
		if (row->getString(3).empty())
		{
			json["enabled"] = row->getString(4);
		}
		else
		{
			JsonElement item = arr[res++];

			item["id"] = row->getString(0);
			item["url"] = row->getString(1);
			item["icon"] = row->getString(2);
			item["title"] = row->getString(3);
			item["enabled"] = row->getString(4);
			item["statetime"] = row->getString(5);
		}
	}

	json["code"] = res;
	out << json;
	
	return XG_OK;
}